from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.views.generic import (
    ListView,
    DetailView,
    DeleteView,
    UpdateView,
    CreateView,
)
from django.shortcuts import redirect
from django.db import IntegrityError
from django.urls import reverse_lazy, reverse


class TodoListView(ListView):
    model = TodoList
    template_name = "list.html"
    context_object_name = "todo_lists"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "detail.html"
    context_object_name = "todo_detail"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tasks"] = TodoList.objects.all()
        return context


def my_TodoCreateView(request):
    if request.method == "GET":
        return render(request, "new.html")
    else:
        name = request.POST.get("name")

        try:
            new_list = TodoList.objects.create(name=name)
        except IntegrityError:
            pass

    return redirect("todo_list_detail", pk=new_list.id)


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "delete.html"

    def get_success_url(request):
        return reverse("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "items_create.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


# def my_TodoCreateView(request):
#     if request.method == "POST" and TodoForm:
#         form = TodoForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect("todo_list_detail", pk=form.id)
#     elif TodoForm:
#         form = TodoForm()
#     else:
#         form = None
#     context = {"form": form}
#     return render(request, "new.html", context)
